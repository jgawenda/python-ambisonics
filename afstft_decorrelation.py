import sys
import os
import scipy
from scipy import signal
from scipy import linalg
import numpy as np
from numpy import matlib
from tqdm import tqdm
import pickle
import soundfile as sf
import spaudiopy
import spaudiopy as spa
from matplotlib import pyplot as plt
import matlab
import matlab.engine

def extract_transients(sig_in, alpha, beta, trans_det1, trans_det2):
    n_sigs, n_frames, n_bins = np.shape(sig_in)

    sig_out = np.zeros((n_sigs, n_frames, n_bins), dtype=np.complex128)

    td1 = trans_det1
    td2 = trans_det2

    for frame in range(n_frames):
        detector_ene = np.power(np.abs(sig_in[:, frame, :]), 2)
        td1 = td1 * alpha
        selector = td1 < detector_ene
        td1[selector] = detector_ene[selector]

        td2 = td2 * beta + (1 - beta) * td1
        selector = td2 > td1
        td2[selector] = td1[selector]

        trans_eq = np.minimum(1, 4 * td2 / (td1 + 1e-20))

        sig_out[:, frame, :] = sig_in[:, frame, :] * trans_eq

    return (sig_out, td1, td2)


def get_decorrelation_delays(n_sigs, band_freqs, sr, 
                             max_delay_frames, hop_size):
    # print(sr)
    n_bins = np.size(band_freqs)
    max_ms = np.min([80, ((max_delay_frames - 1) * hop_size / sr * 1000)])
    delay_max = np.maximum(7, 
        np.minimum(max_ms, 50 * 1000 / (band_freqs + 1e-20)))
    delay_min = np.maximum(3, 
        np.minimum(20, 10 * 1000 / (band_freqs + 1e-20)))
    normalized_delays = np.matlib.repmat(np.arange(n_sigs)\
            / n_sigs, n_bins, 1) + np.random.rand(n_bins, n_sigs) / n_sigs

    print("delay_max:", np.shape(delay_max))
    print("max delay_max:", np.max(delay_max))
    print("delay_min:", np.shape(delay_min))
    print("max delay_min:", np.max(delay_min))
    print("norm_del:", np.shape(normalized_delays))
    for f_bin in range(n_bins):
        perm = np.random.permutation(n_sigs)
        normalized_delays[f_bin, :] = normalized_delays[f_bin, perm]

    delays = normalized_delays * np.matlib.repmat(delay_max - \
            delay_min, n_sigs, 1).T + \
            np.matlib.repmat(delay_min, n_sigs, 1).T

    delays_in_frames = np.rint(delays / 1000 * sr / hop_size)
    delays_in_frames = np.asarray(delays_in_frames, dtype=np.int64)

    return delays_in_frames.T

sh_order = 1
n_sigs = (sh_order + 1) ** 2
degree = 10
t_degree = (sh_order + 1) * 2

nperseg = 2048
hop_size = 128
noverlap = nperseg - hop_size

mode = "l"

xyz_grid = spaudiopy.grids.load_n_design(degree * 8)
n_points = int(np.size(xyz_grid)/3)

azi, colat, r = spa.utils.cart2sph(*xyz_grid.T)
doa_grid = np.vstack([azi, colat]).T

sh_data = None
source_data = None
sr = None

eng = matlab.engine.start_matlab("-nodesktop")

dirs = eng.load("./resources/Lspkr_example.mat")
dirs = np.squeeze(np.asarray(dirs["ls_dirs_deg_aziElev"]))
print("dirs:", dirs)

# vecs = spaudiopy.grids.load_t_design(12)
x, y, z = spa.utils.sph2cart(*dirs.T)
vecs = np.vstack([x, y, z]).T
ls_setup = spa.decoder.LoudspeakerSetup(*vecs.T)
ls_setup.ambisonics_setup(update_hull=False)
n_ls = ls_setup.npoints

stuff = eng.afSTFT(float(128), float(4), float(4), "hybrid")

dirs = dirs / 180 * np.pi
xyz_grid_vls = spaudiopy.grids.load_n_design(degree)
azi_vls, colat_vls, r = spa.utils.cart2sph(*xyz_grid_vls.T)
doa_grid_vls = np.vstack([azi_vls, colat_vls]).T
vls_setup = spa.decoder.LoudspeakerSetup(*xyz_grid_vls.T)
vls_setup.ambisonics_setup(update_hull=False)
n_vls = vls_setup.npoints

sh_data, sr = sf.read("./resources/ambient_sigs.wav")
sh_data = sh_data.T

f = eng.load("./resources/afSTFTcenterfreqs133.mat", nargout=1)
f = np.squeeze(np.asarray(f["afCenterFreq44100"]))

n_bins = 133
block_size = 2048

n_blocks = int(np.ceil(np.shape(sh_data)[1] / nperseg))
n_frames_in_block = int(2048 / 128)
print("number of blocks:", n_blocks)

afSTFTdelay = 12 * hop_size
output_block = np.zeros((4, n_frames_in_block, n_bins), dtype=np.complex128)
output_signals_ls = np.zeros(((afSTFTdelay + block_size * (n_blocks + 1)), 4))
print("output signals shape:", np.shape(output_signals_ls))
n_blocks_in_buffer = int(np.ceil((32 + n_frames_in_block)/ n_frames_in_block))
nbib_1 = n_blocks_in_buffer - 1

ambient_block = np.zeros((4, n_frames_in_block, n_bins), dtype=np.complex128)
ambient_buffer_x_blocks = np.zeros((4, n_blocks_in_buffer * n_frames_in_block, 133), dtype=np.complex128)
dec_ambient_block = np.zeros((4, n_frames_in_block), dtype=np.complex128)

# delays = eng.load("delays.mat", nargout=1)
# delays = np.asarray(delays["delays"])
delays = get_decorrelation_delays(n_sigs, f, sr, n_frames_in_block * 2, hop_size)

alpha = 0.95
beta = 0.995
td1 = np.zeros((4, n_bins), dtype=np.complex128)
td2 = np.zeros((4, n_bins), dtype=np.complex128)

print(nbib_1)
print(n_frames_in_block)

start_index = 0
block_index = 0

for block in tqdm(range(int(n_blocks) - 1)):
    TD_frame = sh_data[:, (block * nperseg):((block + 1) * nperseg)].T
    TD_frame_mat = matlab.double(TD_frame.tolist())
    temp = eng.afSTFT(TD_frame_mat)
    temp_np = np.asarray(temp)

    temp_np = np.moveaxis(temp_np, [0, 1, 2], [1, 2, 0])
    n_sigs, n_bins, n_frames = np.shape(temp_np)
    for frame in range(n_frames_in_block):
        for f_bin in range(n_bins):
            ambient_block[:, frame, f_bin] = temp_np[:, f_bin, frame]

    ambient_buffer_x_blocks[:, 0:(nbib_1 * n_frames_in_block - 1), :] = \
            ambient_buffer_x_blocks[:, (n_frames_in_block):-1, :]

    ambient_buffer_x_blocks[:, (nbib_1 * n_frames_in_block - 1):-1, :], \
    td1, td2 = extract_transients(ambient_block, alpha, beta, td1, td2)

    for f_bin in range(n_bins):
        for ch in range(4):
            dec_delay = int(delays[ch, f_bin])
            dec_ambient_block[ch, :] = ambient_buffer_x_blocks[ch,
                (nbib_1 * n_frames_in_block - 1 - dec_delay):(-1 - dec_delay), f_bin]

        output_block[:, :, f_bin] = dec_ambient_block

    temp_out = np.moveaxis(output_block, [0, 1, 2], [2, 1, 0])
    temp_out = matlab.double(temp_out.tolist(), is_complex=True)

    temp_out = np.asarray(eng.afSTFT(temp_out, nargout=1))
    output_signals_ls[(block * nperseg):((block + 1) * nperseg), :] = \
            temp_out

output_signals_ls = output_signals_ls[(afSTFTdelay - 1):(afSTFTdelay + (n_blocks - 1) * block_size - 1), :]
output_signals_ls = 0.5 * output_signals_ls/np.max(np.max(np.abs(output_signals_ls)))
sf.write("dec_test.wav", output_signals_ls, sr, format = "WAV")
