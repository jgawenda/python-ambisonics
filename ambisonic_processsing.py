
# USAGE:
# python3 <path to pkl file containing ambisonic data> <optional: path to file containing NTF source data> <output mode> <stream balance> <decoding bal>
# output mode = 'h' for headphones rendering and 'l' for loudspeaker rendering
# stream balance  = <0, 2>, 0 -- for ambient sounds only, 2 for source sounds only, value in between for both
# decoding balance = <0, 1>, 0 -- for linear ambisonic decoding, 1 for COMPASS decoding


import sys
import os
import scipy
from scipy import signal
from scipy import linalg
import numpy as np
from numpy import matlib
# from numpy import random
from tqdm import tqdm
import pickle
import soundfile as sf
import spaudiopy
import spaudiopy as spa
from matplotlib import pyplot as plt
import tikzplotlib

def get_decoding_matrix(vls_dirs, sh_order, sr, n_bins, n_ls, n_vls, mode="h", ls_setup = None, sh_type="real"):
    hrirs = spaudiopy.IO.load_hrirs(fs=sr)
    Y_vls = np.conj(get_sh(sh_order, vls_dirs, sh_type=sh_type)).T
    G_vls = None

    if (mode == "h"):
        G_vls = np.zeros((n_bins, 2, n_vls), 
                dtype=np.complex128)
        for ls in range(n_ls):
            current_hrir = hrirs.nearest_hrirs(vls_dirs[ls, 0], 
                    vls_dirs[ls, 1])
            # print(np.shape(stuff))
            hrtf_left = np.abs(scipy.fft.rfft(current_hrir[0], 
                    n = n_bins * 2 - 1))
            hrtf_right = np.abs(scipy.fft.rfft(current_hrir[1], 
                    n = n_bins * 2 - 1))
            
            G_vls[:, 0, ls] = hrtf_left
            G_vls[:, 1, ls] = hrtf_right

    if (mode == "l"):
        G_vls = np.zeros((n_bins, n_ls, n_vls),
                dtype=np.complex128)

        xyz_grid = np.vstack([*spa.utils.sph2cart(*vls_dirs.T)]).T
        G_vls[:] = np.conj(spa.decoder.vbap(xyz_grid, ls_setup)).T

    D = np.einsum("ijk, kl -> ijl", G_vls, Y_vls)
    D = D / n_ls

    return (D, G_vls, Y_vls)

def get_adaptive_matrix(source_dirs, W_s, D, n_dirs = 3, erb_idx = None, ls_setup = None, mode = "h"):
    hrirs = spaudiopy.IO.load_hrirs(fs=sr)
    n_bins = np.shape(D)[0]
    n_dirs = np.max(n_dirs)
    n_ls = None
    A_s = None
    G_s = None
    
    if (mode == "h"):
        n_ls = 2
        G_s = np.zeros((n_bins, n_ls, n_dirs), dtype=np.complex128)
        A_s = np.zeros((n_bins, n_ls, n_ls), dtype=np.complex128)

        for s in range(n_dirs):
            for erb in range(np.shape(erb_idx)[0] - 1):
                erb_bins = list(range(erb_idx[erb], erb_idx[erb + 1]))
                current_hrir = hrirs.nearest_hrirs(source_dirs[s, erb_bins[0], 0], 
                        source_dirs[s, erb_bins[0], 1])
                hrtf_left = scipy.fft.rfft(current_hrir[0], 
                        n = n_bins * 2 - 1)
                hrtf_right = scipy.fft.rfft(current_hrir[1], 
                        n = n_bins * 2 - 1)
                G_s[erb_bins, 0, s] = hrtf_left[erb_bins]
                G_s[erb_bins, 1, s] = hrtf_right[erb_bins]

    if (mode == "l"):
        n_ls = ls_setup.npoints
        G_s = np.zeros((n_bins, n_ls, n_dirs), dtype=np.complex128)
        A_s = np.zeros((n_bins, n_ls, n_ls), dtype=np.complex128)
        for erb in range(np.shape(erb_idx)[0] - 1):
            erb_bins = list(range(erb_idx[erb], erb_idx[erb + 1]))

            xyz_grid = np.vstack([*spa.utils.sph2cart(*source_dirs[:n_dirs, erb_bins[0]].T)]).T
            vbap_gains = np.conj(spa.decoder.vbap(xyz_grid, ls_setup)).T
            G_s[erb_bins] = vbap_gains

    for f_bin in range(n_bins):
        A_s[f_bin] = G_s[f_bin] @ W_s[f_bin] @ np.conj(D[f_bin]).T \
                @ np.linalg.pinv(D[f_bin] @ np.conj(D[f_bin]).T)

    return A_s
    
def get_synthesis_matrix(A_s, W_d, D, dec_bal, stream_bal, n_ls=2, n_sigs=4):
    g_src = None
    g_diff = None

    if (stream_bal < 1):
        g_src = stream_bal
        g_diff = 1
    elif (stream_bal >= 1 and stream_bal <= 2):
        g_src = 1
        g_diff = 2 - stream_bal

    gs = 1
    gd = 2 / np.sqrt(np.shape(D)[2])

    n_bins = np.shape(W_d)[0]
    M = np.zeros((n_bins, n_ls, n_sigs), dtype=np.complex128)
    M_s = np.zeros((n_bins, n_ls, n_sigs), dtype=np.complex128)
    M_d = np.zeros((n_bins, n_ls, n_sigs), dtype=np.complex128)

    for f_bin in range(n_bins):
        M_s[f_bin] = g_src * (gs * dec_bal * A_s[f_bin] + (1 - dec_bal) * np.eye(n_ls)) @ D[f_bin]
        M_d[f_bin] = g_diff * D[f_bin] @ (gd * dec_bal * W_d[f_bin] + (1 - dec_bal) * np.eye(4))

    M = M_s + M_d

    return (M_s, M_d, M)

def get_sorted_eigs(C):
    eig_val, eig_vec = scipy.linalg.eigh(C)

    sorted_idx = np.flip(np.argsort(eig_val))
    sorted_eig_val = eig_val[sorted_idx]
    sorted_eig_vec = eig_vec[:, sorted_idx]

    return (sorted_eig_val, sorted_eig_vec)

def find_peaks(pmu, doa_grid, xyz_grid, n_peaks, kappa = 100):
    new_pmu = np.copy(pmu)
    peak_idx = np.zeros((n_peaks), dtype=np.int32)

    for k in range(n_peaks - 1):
        peak_idx[k] = np.argmax(new_pmu)
        VM_mean = xyz_grid[peak_idx[k], :]
        VM_mask = kappa / (2 * np.pi * np.exp(kappa) - np.exp(-kappa)) \
                * np.exp(kappa * np.einsum("ij, j -> i", xyz_grid, np.conj(VM_mean).T))
        VM_mask = 1 / (0.0001 + VM_mask)
        new_pmu = new_pmu * VM_mask

    peak_idx[n_peaks - 1] = np.argmax(new_pmu)
    return peak_idx

# calculate angle beteween two directions of arrival
def calculate_angle(doa1, doa2):
    v1 = doa2cart(doa1)
    v2 = doa2cart(doa2)

    dot = np.dot(v1, v2)
    dot = np.round(dot, 5)

    ang = np.arccos(dot)

    return ang

def calculate_error_and_estimate(org_doa, est_doa, n_est):

    error = np.zeros((n_sigs - 1))
    est_source = np.ones((n_sigs - 1), dtype=np.int32) * -1
    if (n_est == 0):
        return 0
    if (n_est == 1):
        # est_doa = est_doa[0]
        if (len(np.shape(est_doa)) != 1):
            est_doa = est_doa[0]
        if (np.size(org_doa) == 3):
            dots = np.einsum("i, i",
                    org_doa,
                    est_doa.T)
            dots = np.clip(dots, -1, 1)
            angle = np.round(np.arccos(dots), 10) * 180 / np.pi
            error[0] = angle
            est_source[0] = 0
            return (est_source, error)
        else:
            dots = np.einsum("ij, j -> i",
                    org_doa,
                    est_doa.T)
            dots = np.clip(dots, -1, 1)
            angles = np.round(np.arccos(dots), 10) * 180 / np.pi
            idx = np.unravel_index(angles.argmin(), angles.shape)
            error[idx[0]] = angles[idx]
            est_source[0] = idx[0]
            return (est_source, error)
    else:
        dots = np.einsum("ij, jl -> il", org_doa, est_doa.T)
        dots = np.clip(dots, -1, 1)
        angles = np.round(np.arccos(dots), 10) * 180 / np.pi
        i = 0

        # while (angles.size != 0):
        while (i < n_est):
            idx = np.unravel_index(angles.argmin(), angles.shape)
            # sum_error += angles[idx] * 180 / np.pi
            error[idx[0]] = angles[idx]
            est_source[i] = idx[0]

            angles[idx[0], :] += 200
            angles[:, idx[1]] += 200

            i += 1
        # print(error)
        return (est_source, error)

def calculate_error(org_doa, est_doa, n_est):

    error = np.zeros((n_sigs - 1))
    if (n_est == 0):
        return 0
    if (n_est == 1):
        # est_doa = est_doa[0]
        if (len(np.shape(est_doa)) != 1):
            est_doa = est_doa[0]
        if (np.size(org_doa) == 3):
            dots = np.einsum("i, i",
                    org_doa,
                    est_doa.T)
            dots = np.clip(dots, -1, 1)
            angle = np.round(np.arccos(dots), 10) * 180 / np.pi
            error[0] = angle
            return error
        else:
            dots = np.einsum("ij, j -> i",
                    org_doa,
                    est_doa.T)
            dots = np.clip(dots, -1, 1)
            angles = np.round(np.arccos(dots), 10) * 180 / np.pi
            idx = np.unravel_index(angles.argmin(), angles.shape)
            error[0] = angles[idx]
            return error
    else:
        dots = np.einsum("ij, jl -> il", org_doa, est_doa.T)
        dots = np.clip(dots, -1, 1)
        angles = np.round(np.arccos(dots), 10) * 180 / np.pi
        i = 0

        # while (angles.size != 0):
        while (i < n_est):
            idx = np.unravel_index(angles.argmin(), angles.shape)
            # sum_error += angles[idx] * 180 / np.pi
            error[i] = angles[idx]

            angles[idx[0], :] += 200
            angles[:, idx[1]] += 200

            i += 1

        # print(error)
        return error

def generate_doa_grid(n_points):
    doa_grid = np.zeros((n_points, 2))
    xyz_grid = np.zeros((n_points, 3))
    phi = np.pi * (3. - np.sqrt(5.))

    print("Generating DoA grid with {} points".format(n_points))
    for i in range(n_points):
        y = 1 - (i / float(n_points - 1)) * 2
        radius = np.sqrt(1 - y * y)

        theta = phi * i

        x = np.cos(theta) * radius
        z = np.sin(theta) * radius

        doa_grid[i, :] = cart2doa(np.asarray([x, y, z]))
        xyz_grid[i, :] = np.asarray([x, y, z])

    return (doa_grid, xyz_grid)

def get_sh(sh_order, doas, sh_type = "real"):
    n_sigs = (sh_order + 1)**2
    n_dirs = np.shape(doas)[0]

    if (np.size(doas) == 2 and len(np.shape(doas)) == 1):
        Y = spaudiopy.sph.sh_matrix(sh_order, doas[0], doas[1], sh_type)
    elif (np.size(doas) == 2 and len(np.shape(doas)) == 2):
        Y = spaudiopy.sph.sh_matrix(sh_order, doas[0, 0], doas[0, 1], sh_type)
    else:
        Y = spaudiopy.sph.sh_matrix(sh_order, doas[:, 0], doas[:, 1], sh_type)

    return np.conj(Y).T

def get_post_filter(source_spectra):
    n_sources, n_bins, n_frames = np.shape(source_spectra)

    source_powers = np.abs(source_spectra)
    sum_power = np.sum(source_powers, axis=0)
    tmp = np.zeros(np.shape(source_spectra))

    for i in range(n_sources):
        tmp[i] = source_powers[i] / sum_power

    return tmp

def find_erb_partitions(band_freqs, max_freq_limit):

    if (max_freq_limit > 20000):
        max_freq_limit = 20000

    band_center_freq  = (np.power(2, 1/3) + 1) / 2
    # erb_freqs = band_freqs[0]
    erb_freqs = np.zeros(np.shape(band_freqs))
    erb_freqs[0] = band_freqs[0]
    erb_index = np.zeros(np.shape(band_freqs), dtype=np.int32)
    erb_index[0] = 0

    counter = 0
    while erb_freqs[counter] < max_freq_limit:
        erb = 24.7  + 0.108 * erb_freqs[counter] * band_center_freq
        erb_freqs[counter + 1] = erb_freqs[counter] + erb

        erb_index[counter + 1] = np.argmin(np.abs(erb_freqs[counter + 1] - band_freqs))
        if (erb_index[counter + 1] == erb_index[counter]):
            erb_index[counter + 1] = erb_index[counter + 1] + 1

        erb_freqs[counter + 1] = band_freqs[erb_index[counter + 1]]

        counter += 1

    erb_freqs[counter + 1] = band_freqs[-1]
    erb_index[counter + 1] = len(band_freqs)

    erb_freqs = erb_freqs[:(counter + 2)]
    erb_index = erb_index[:(counter + 2)]

    return (erb_index, erb_freqs)

# function to convert spherical coordinates to cartesian
def doa2cart(doa):
    if (len(np.shape(doa)) == 1):
        x = np.cos(doa[0]) * np.sin(doa[1])
        y = np.sin(doa[0]) * np.sin(doa[1])
        z = np.cos(doa[1])
        cart = np.asarray([x, y, z])
    else:
        n_doas = np.shape(doa)[0]
        cart = np.zeros((n_doas, 3))
        for i in range(n_doas):
            x = np.cos(doa[i, 0]) * np.sin(doa[i, 1])
            y = np.sin(doa[i, 0]) * np.sin(doa[i, 1])
            z = np.cos(doa[i, 1])

            cart[i] = np.asarray([x, y, z])

    return cart

def cart2doa(vec):
    theta = np.arctan2(vec[1], vec[0])
    phi = np.arccos(vec[2])

    doa = np.asarray([theta, phi])

    return doa

# SORTE method for estimating a number of sources
def sorte(W, n_sigs):
    # W -- sorted array of eigenvalues (descending order)
    # n_sigs -- number of signals/microphones/etc.

    K = -1

    N = np.shape(W)[0]
    d_w = np.zeros((n_sigs - 1), dtype=np.complex128)
    sig_2 = np.zeros((n_sigs - 1), dtype=np.complex128)
    o_k = np.zeros((n_sigs - 2), dtype=np.complex128)

    # else:
    d_w = W[:-1] - W[1:]
    for k in range(n_sigs - 1):
        mean_d = 1/(n_sigs - (k + 1)) * np.sum(d_w[k:])
        sig_2[k] = 1/(n_sigs - (k + 1)) * np.sum(np.power(d_w[k:] - mean_d, 2))

    for k in range(n_sigs - 2):
        if (sig_2[k] > 0):
            o_k[k] = sig_2[k + 1] / sig_2[k]
        elif (sig_2[k] == 0):
            o_k[k] = np.inf

    o_k = np.round(o_k, 5)

    K = np.argmin(o_k) + 1

    return K

def get_beamforming_matrices(
        n_bins,
        est_doas,
        est_k,
        sh_order,
        est_source = None,
        post_filter = None,
        erb_idx = None,
        ):

    n_sigs = (sh_order + 1)**2
    W_d = np.zeros((n_bins, n_sigs, n_sigs), dtype=np.complex128)

    max_k = 0
    g = None
    g_diff = None
    g_mask = None

    if (np.size(est_k) == 1):
        max_k = est_k
        est_k = np.ones((n_bins), dtype=np.int32) * max_k
    else:
        max_k = np.max(est_k)


    if (np.size(est_doas) == 2):
        est_doas = np.ones((max_k, n_bins, 2)) * est_doas
    elif (np.size(est_doas) == 2 * max_k):
        new_doas = np.ones((max_k, n_bins, 2))
        for f_bin in range(n_bins):
            new_doas[:, f_bin] = est_doas
        est_doas = new_doas

    W_s = np.zeros((n_bins, max_k, n_sigs), dtype=np.complex128)

    if (erb_idx is None):
        for f_bin in range(n_bins):
            if (est_k[f_bin] == 0):
                print("No detected sources")
                continue

            Y_s = get_sh(sh_order,
                    est_doas[:est_k[f_bin], f_bin],
                    )
            W_bin = np.linalg.pinv(Y_s)

            if (post_filter is None):
                g = np.eye(est_k[f_bin])
            else:
                g = np.diag(post_filter[:max_k, f_bin])

            if (est_source is not None):
                g_mask = np.diag(np.zeros(est_k[f_bin]))
                if (np.size(est_source) == 1):
                    g_mask = 1
                else:
                    for k in range(est_k[f_bin]):
                        if (est_source[k, f_bin] != -1):
                            g_mask[est_source[k, f_bin], est_source[k, f_bin]] = 1
            else:
                g_mask = np.eye(est_k[f_bin])

            g = g * g_mask

            W_d[f_bin, :, :] = np.eye(n_sigs) - Y_s @ g @ W_bin
            W_s[f_bin, :, :] = g @ W_bin

    return (W_s, W_d)

def get_pmu(eig_noise, Y):
    VnA = np.einsum("ij, jk -> ik", np.conj(eig_noise.T), Y)
    Pmu = 1 / np.sum(np.conj(VnA) * VnA, axis=0)
    return Pmu

argc = len(sys.argv) - 1
processing_mode = "none"

source_file = None
ntf_file = None
mode = None
stream_bal = None
dec_bal = None
out_type = None
n_sources = None

if (argc == 4):
    print("Processing mode is set to regular COMPASS")
    processing_mode = "reg"
    source_file = sys.argv[1]
    mode = sys.argv[2]
    stream_bal = float(sys.argv[3])
    dec_bal = float(sys.argv[4])

    out_type = "regular_"
elif (argc == 5):
    print("Processing mode is set to NTF + COMPASS")
    processing_mode = "ntf"
    source_file = sys.argv[1]
    ntf_file = sys.argv[2]
    mode = sys.argv[3]
    stream_bal = float(sys.argv[4])
    dec_bal = float(sys.argv[5])
    # n_sources = float(sys.argv[6])

    out_type = "ntf_"

# spherical harmonics parameters
sh_order = 1
n_sigs = (sh_order + 1) ** 2

# grid generation parameters
degree = 10
t_degree = (sh_order + 1) * 2

# MUSIC grid generation
xyz_grid = spaudiopy.grids.load_n_design(degree * 8)
n_points = int(np.size(xyz_grid)/3)
azi, colat, r = spa.utils.cart2sph(*xyz_grid.T)
doa_grid = np.vstack([azi, colat]).T
Y_grid = get_sh(sh_order, doa_grid) # SH coefficients for DoA grid

# loudspeaker setup
vecs = spaudiopy.grids.load_t_design(3)
ls_setup = spa.decoder.LoudspeakerSetup(*vecs.T)
ls_setup.ambisonics_setup(update_hull=False)

n_ls = None
if (mode == "h"):
    n_ls = 2
if (mode == "l"):
    n_ls = ls_setup.npoints

# virtual loudspeaker setup for diffuse sound synthesis
xyz_grid_vls = spaudiopy.grids.load_n_design(degree)
azi_vls, colat_vls, r = spa.utils.cart2sph(*xyz_grid_vls.T)
doa_grid_vls = np.vstack([azi_vls, colat_vls]).T
vls_setup = spa.decoder.LoudspeakerSetup(*xyz_grid_vls.T)
vls_setup.ambisonics_setup(update_hull=False)

# STFT parameters
nperseg = 2048
hop_size = 512
noverlap = nperseg - hop_size

# temporal smoothing parameters
tau = 0.2
interp = np.exp(-hop_size / (44100 * tau))

sh_data = None
source_data = None
sr = None

file_out = None
# file_out = out_type

# file_out = filename + out_type

# rng = np.random.default_rng()

print("mode:", mode)
print("stream bal:", stream_bal)
print("dec bal:", dec_bal)

# ntf_path = "./data/NTF_output/" + filename + "/0.pkl"
# ambi_path = "./data/simulated_ambisonic_recordings/" + filename + "0.pkl"

est_doa_org = None
ntf_data = None
ambi_data = None


# if ("Mozart" in filename):
#     source_path = "./data/source_signals/Mozart_40th_symphony/tl_15_nos_3_msar_80_ftg_1_s_True_fs_44100/0.pkl"
#     with open(source_path, "rb") as f:
#         source_data = pickle.load(f)
# elif ("Eddie" in filename):
#     source_path = "./data/source_signals/Eddie_Garrido_Scarlett/tl_15_nos_3_msar_80_ftg_1_s_True_fs_44100/0.pkl"
#     with open(source_path, "rb") as f:
#         source_data = pickle.load(f)

# with open(ambi_path, "rb") as f:
#     ambi_data = pickle.load(f)

with open(source_file, "rb") as f:
    ambi_data = pickle.load(f)

sr = ambi_data["fs"]
sh_sigs = np.asarray(ambi_data["s"])
# source_type = source_data["source_type"]
sh_data = np.sum(sh_sigs, axis=0)

# print(ntf_data.keys())
# print(source_data.keys())
# print(ambi_data.keys())

# print(np.shape(ambi_data["s"]))

if (processing_mode == "ntf"):
    with open(ntf_file, "rb") as f:
        ntf_data = pickle.load(f)

    source_sigs = np.asarray(ntf_data["y"])
    source_sigs = source_sigs[:, 0, :]
    n_sources = np.shape(source_sigs)[0]
    org_doa = np.asarray(ntf_data["doa"])

    est_doa_org = np.asarray([cart2doa(org_doa[0]), cart2doa(org_doa[1]), cart2doa(org_doa[2])])


X = None
Z = None
post_filter = None
f, t, X = scipy.signal.stft(sh_data,
        fs = sr,
        nperseg = nperseg,
        noverlap = noverlap,
        return_onesided=True,
        window="hann"
        )

if (processing_mode == "ntf"):
    f, t, Z = scipy.signal.stft(source_sigs,
            fs = sr,
            nperseg = nperseg,
            noverlap = noverlap,
            return_onesided=True,
            window="hann"
            )

    post_filter = get_post_filter(Z)


erb_idx, erb_freqs = find_erb_partitions(f, 18000)
n_erbs = len(erb_idx)

_, n_bins, n_frames = np.shape(X)

C = np.zeros((n_sigs, n_sigs, n_bins, n_frames), dtype=np.complex128)

Pmus = np.zeros((n_frames, n_points))
est_doas = np.zeros((n_frames, n_sigs - 1, n_bins, 2))

est_k = np.zeros((n_frames, n_bins), dtype=np.int32)
est_source = np.ones((n_sigs - 1, n_frames, n_bins), dtype=np.int32) * -1
error = np.zeros((n_sigs - 1, n_frames, n_bins))

n_ls = None

if (mode == "l"):
    n_ls = ls_setup.npoints
else:
    n_ls = 2

Dec, G_vls, Y_vls = get_decoding_matrix(doa_grid_vls, sh_order, sr, n_bins, n_ls = n_ls, n_vls = vls_setup.npoints,
        mode=mode, sh_type="real", ls_setup = ls_setup)

max_K = n_sigs - 1
est_k = np.ones((n_frames, n_bins), dtype=np.int32)
X_source = np.zeros((3, n_bins, n_frames))
A = np.zeros((max_K, n_bins, n_frames), dtype=np.complex128)
C_prev = np.zeros((n_sigs, n_sigs), dtype=np.complex128)

# print("performing compass analysis")
# print("X:", np.shape(X))
# print("org_doa", spa.utils.cart2sph(*org_doa.T))
# a = np.round(a, 2)
for frame in tqdm(range(n_frames)):
    if (processing_mode == "reg"):
        Cx = np.zeros((n_sigs, n_sigs, n_bins), dtype=np.complex128)

        for f_bin in range(n_bins):
            tmp = np.einsum("i, j -> ij", X[:, f_bin, frame], np.conj(X[:, f_bin, frame]).T)
            Cx[:, :, f_bin] = tmp

        for erb in range(n_erbs - 1):
            erb_bins = list(range(erb_idx[erb], erb_idx[erb + 1]))

            if (interp == 1):
                if (frame != 0):
                    C[:, :, erb_bins, frame] = C[:, :, erb_bins, frame - 1] + \
                            Cx[:, :, erb_bins]
                else:
                    C[:, :, erb_bins, frame] = Cx[:, :, erb_bins]
            else:
                if (frame != 0):
                    C[:, :, erb_bins, frame] = interp * C[:, :, erb_bins, frame - 1] + \
                            (1 - interp) * Cx[:, :, erb_bins]
                else:
                    C[:, :, erb_bins, frame] = (1 - interp) * Cx[:, :, erb_bins]

            C_erb = np.sum(C[:, :, erb_bins, frame], axis=2)

            eig_val, eig_vec = get_sorted_eigs(C_erb)

            K = sorte(eig_val, n_sigs)

            if (K != 0):
                if (K > max_K):
                    max_K = K

                eig_noise = eig_vec[:, K:]
                Pmu = get_pmu(eig_noise, Y_grid)

                doa_idx = find_peaks(Pmu, doa_grid, xyz_grid, K, kappa = 50)
                est_doas[frame, :K, erb_bins] = doa_grid[doa_idx]

            est_k[frame, erb_bins] = int(K)
    if (processing_mode == "ntf"):
        for erb in range(n_erbs - 1):
            erb_bins = list(range(erb_idx[erb], erb_idx[erb + 1]))
            K = n_sources
            est_doas[frame, :K, erb_bins] = est_doa_org
            est_k[frame, erb_bins] = int(K)

R_cur = None

S = np.zeros((n_ls, n_bins, n_frames), dtype=np.complex128)

for frame in tqdm(range(n_frames)):
    try:
        if (processing_mode == "reg"):
            W_s, W_d = get_beamforming_matrices(
                            n_bins,
                            est_doas[frame],
                            est_k[frame],
                            sh_order,
                            )
        if (processing_mode == "ntf"):
            W_s, W_d = get_beamforming_matrices(
                            n_bins,
                            est_doas[frame],
                            est_k[frame],
                            sh_order,
                            post_filter = post_filter[:, :, frame],
                            )
    except IndexError:
        break

    A_s = get_adaptive_matrix(est_doas[frame], W_s, Dec, n_dirs = est_k[frame],
            ls_setup=ls_setup, mode=mode, erb_idx=erb_idx)

    R_s, R_d, R = get_synthesis_matrix(A_s, W_d, Dec, dec_bal = dec_bal, 
            stream_bal = stream_bal, n_ls = n_ls)

    if (frame == 0):
        R_cur = (1 - interp) * R
    else:
        R_cur = interp * R_prev + (1 - interp) * R

    S[:, :, frame] = np.einsum("ijk, ki -> ij", R_cur, X[:, :, frame]).T
    R_prev = R_cur

t, s = scipy.signal.istft(S, fs = sr, time_axis = 2, freq_axis = 1,
        nperseg = nperseg, noverlap = noverlap,
        window="hann",
        input_onesided=True)

if (mode == "h"):
    # l, r = ls_setup.binauralize(s, fs = sr)
    # s = np.asarray([l, r])
    file_out = out_type + "headphones"
else:
    file_out = out_type + "loudspeakers"

s = s / (2 * np.max(np.max(s)))

sf.write(file_out + ".wav", s.T * 1, sr, format = "WAV")
